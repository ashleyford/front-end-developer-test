# Front-end dev test

We would like you to build the charity donation component from the image ([charity-donation-component.png](charity-donation-component.png?raw=true)) contained in this package. There is no right or wrong way to go about it. We want to see your approach to HTML, CSS, and JavaScript. The design is a bit static, so feel free to make it come to life a little. Have fun with it!

## Deliverable

We would like to see the component you built in action, so sending us the build folder would be great. However, we are equally interested in your approach to building the component, so please also include all your source files for example all your SCSS or LESS files, gulp, and yarn configurations.

Even better: create a repository to work on your component, and give us access to it!

### Good luck, and have fun coding!
